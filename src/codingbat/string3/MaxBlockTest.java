package codingbat.string3;

/*Given a string, return the length of the largest "block" in the string.
A block is a run of adjacent chars that are the same.*/
public class MaxBlockTest {
    public static void main(String[] args) {
        System.out.println("maxBlock(\"XXBBBBbbxx\") → " + maxBlock("XXBBBBbbxx"));
        System.out.println("maxBlock(\"hoopla\") → " + maxBlock("hoopla"));
        System.out.println("maxBlock(\"\") → " + maxBlock(""));
    }

    public static int maxBlock(String str) {
        int maxCount = 1;
        if (str.length() == 0) {
            return 0;
        }
        int count = 1;
        for (int i = 0; i < str.length() - 1; i++) {
            char ch1 = str.charAt(i);
            char ch2 = str.charAt(i + 1);
            if (ch1 == ch2) {
                count++;
                maxCount = maxCount > count ? maxCount : count;
            } else {
                count = 1;
            }
        }
        return maxCount;
    }
}
