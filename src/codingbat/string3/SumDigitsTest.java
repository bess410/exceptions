package codingbat.string3;

/*Given a string, return the sum of the digits 0-9 that appear in the string, ignoring all other
characters. Return 0 if there are no digits in the string. (Note: Character.isDigit(char) tests
if a char is one of the chars '0', '1', .. '9'. Integer.parseInt(string) converts a string to an
int.)*/
public class SumDigitsTest {
    public static void main(String[] args) {
        System.out.println("sumDigits(\"aa1bc2d3\") → " + sumDigits("aa1bc2d3"));
        System.out.println("sumDigits(\"Chocolate\") → " + sumDigits("Chocolate"));
        System.out.println("sumDigits(\"X1z9b2\") → " + sumDigits("X1z9b2"));
    }

    public static int sumDigits(String str) {
        int count = 0;
        for(int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (Character.isDigit(ch)) {
                count += Integer.parseInt(str.substring(i, i + 1));
            }
        }
        return count;
    }
}
