package codingbat.string3;

/*Given a string, return true if the number of appearances of "is" anywhere in the string is
equal to the number of appearances of "not" anywhere in the string (case sensitive).*/
public class EqualIsNotTest {
    public static void main(String[] args) {
        System.out.println("equalIsNot(\"This is not\") → " + equalIsNot("This is not"));
        System.out.println("equalIsNot(\"\") → " + equalIsNot(""));
        System.out.println("equalIsNot(\"isisnotnot\") → " + equalIsNot("isnotisnot"));
    }

    public static boolean equalIsNot(String str) {
        int isCount = 0;
        int notCount = 0;
        int indexIs = str.indexOf("is");
        int indexNot = str.indexOf("not");

        while (indexIs >= 0) {
            isCount++;
            indexIs = str.indexOf("is", indexIs + 2);
        }

        while (indexNot >= 0) {
            notCount++;
            indexNot = str.indexOf("not", indexNot + 3);
        }

        return isCount == notCount;
    }

}
