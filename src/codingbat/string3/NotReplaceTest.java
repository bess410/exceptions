package codingbat.string3;

/*Given a string, return a string where every appearance of the lowercase word "is"
has been replaced with "is not". The word "is" should not be immediately preceeded
or followed by a letter -- so for example the "is" in "this" does not count.
(Note: Character.isLetter(char) tests if a char is a letter.)
*/
public class NotReplaceTest {
    public static void main(String[] args) {
        System.out.println("notReplace(\"is test\") → " + notReplace("is test"));
        System.out.println("notReplace(\"is-is\") → " + notReplace("is-is"));
        System.out.println("notReplace(\"xis yis\") → " + notReplace("xis yis"));
    }

    public static String notReplace(String str) {
        int index = str.indexOf("is", 0);
        while (index >= 0) {
            if (index == 0) {
                if (index == str.length() - 2) {
                    return "is not";
                }
                if (!Character.isLetter(str.charAt(index + 2))) {
                    str = "is not" + str.substring(2);
                }
            } else if (index == str.length() - 2) {
                if (!Character.isLetter(str.charAt(index - 1))) {
                    return str.substring(0, str.length() - 2) + "is not";
                }
            } else if (!Character.isLetter(str.charAt(index - 1)) && !Character.isLetter(str.charAt(index + 2))) {
                str = str.substring(0, index) + "is not" + str.substring(index + 2);
            }

            index = str.indexOf("is", index + 2);
        }
        return str;
    }
}
